#!/bin/bash
mkdir /zcloud_server/
mkfs.xfs /dev/vdb
cat >> /etc/fstab << EOF
/dev/vdb             /zcloud_server          xfs    defaults        0 0 
EOF
mount -a

mkdir -p /zcloud_server/zcloud_3.3.1/dbaas/
mkdir -p /zcloud_server/zcloud_3.3.1/log/
mkdir -p /zcloud_server/mysql/
mkdir -p /zcloud_server/paasdata/
ln -s /zcloud_server/paasdata /paasdata
chmod -R 755 /zcloud_server

mkdir /paasdata/Proxy
mkdir /paasdata/Agent
wget -P /opt/ https://documentation-samples.obs.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/rapid-deployment-of-zcloud-database-management-platform/open-source-software/zcloud.tar.gz
tar xf /opt/zcloud.tar.gz -C /opt/
tar xvf /opt/zCloud_3.3.1_2022043001_Server_Release.tar.gz --strip-components 1 -C /zcloud_server/zcloud_3.3.1/dbaas/
mv /opt/agent_aix_3.3.1_20220429_1608.tar /paasdata/Agent
mv /opt/agent_linux_3.3.1_20220429_1608.tar.gz /paasdata/Agent
mv /opt/proxy_linux_3.3.1_20220430_0009.tar.gz /paasdata/Proxy

cp -a /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak
wget -O /etc/yum.repos.d/CentOS-Base.repo https://repo.huaweicloud.com/repository/conf/CentOS-7-reg.repo
sed -i  '30 i:<<!'  /zcloud_server/zcloud_3.3.1/dbaas/lib/init_for_install.sh
sed -i  '54 i!'  /zcloud_server/zcloud_3.3.1/dbaas/lib/init_for_install.sh
sed -i 's/\(yum.repo.name=\).*/\1base/' /zcloud_server/zcloud_3.3.1/dbaas/zcloudsingle.cfg
sed -i 's/\(log.data.dir=\).*/\1\/zcloud_server\/zcloud_3.3.1\/log/' /zcloud_server/zcloud_3.3.1/dbaas/zcloudsingle.cfg
sed -i 's/\(repo.data.dir=\).*/\1\/paasdata/' /zcloud_server/zcloud_3.3.1/dbaas/zcloudsingle.cfg
sed -i 's/\(mysql.data.dir=\).*/\1\/zcloud_server\/mysql/' /zcloud_server/zcloud_3.3.1/dbaas/zcloudsingle.cfg
ETH0=$(hostname -I|awk '{print $1}');
sed -i  's/\(consul.host=\).*/\1'$ETH0'/' /zcloud_server/zcloud_3.3.1/dbaas/zcloudsingle.cfg

cd /zcloud_server/zcloud_3.3.1/dbaas/ || exit
chmod +x ./*.sh
./install.sh --nodeIp "$ETH0"