[TOC]

**解决方案介绍**
===============
该解决方案能帮助用户快速部快速在华为云上部署zCloud数据库云管理平台，zCloud可以对Oracle、DB2、SQL Server、MySQL、PostgreSQL、Redis、MongoDB、OpenGauss、达梦、MogDB，OceanBase、TiDB、Gbase8s、KingBaseES、OGG数据库进行专业易用的监控、巡检、告警、性能分析、SQL分析等。可以基于华为云上的ECS一键部署Oracle、SQL Server、MySQL、PostgreSQL、Redis、MongoDB、OpenGauss、达梦数据库。

解决方案实践详情页地址：
https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-zcloud-database-management-platform.html

**架构图**
---------------
![方案架构](./document/rapid-deployment-of-zcloud-database-management-platform.png)

**架构描述**
---------------
该解决方案会部署如下资源：

1.创建两台弹性云服务器，分别用作zCloud数据库云管理平台的管理节点和proxy节点。

2.创建一个弹性公网IP，用于提供管理节点访问公网和被公网访问能力。

3.创建安全组，通过配置安全组规则，为弹性云服务器提供安全防护。

**组织结构**
---------------

``` lua
huaweicloud-solution-rapid-deployment-of-zcloud-database-management-platform
├── rapid-deployment-of-zcloud-database-management-platform.tf.json -- 资源编排模板
├── userdate
    ├── zcloud.sh -- 脚本配置文件
```
**开始使用**
---------------
环境初始化和软件配置安装大约用时20分钟，完成之后方可登陆网站和进行密码重置操作。

1、重置密码

登录[华为云服务器控制台](https://console.huaweicloud.com/ecm/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)，区域选择“北京四”，参考官网[重置弹性云服务器密码](https://support.huaweicloud.com/usermanual-ecs/zh-cn_topic_0067909751.html)，修改弹性云服务器初始化密码。

图1 重置密码
![登录ECS云服务器控制平台](./document/readme-image-001.png)

2、查看创建的弹性云服务器实例EIP

在[华为云服务器控制台](https://console.huaweicloud.com/ecm/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)，查看该方案一键部署创建的ECS实例及其绑定的弹性公网IP。

图2 ECS绑定的公网IP
![ECS绑定的公网IP](./document/readme-image-002.png)

3、打开浏览器，输入“http://EIP:80”， 即可访问网站。

图3 访问网站
![访问网站](./document/readme-image-003.png)

4、详细使用请参考华为云官网该解决方案的部署指南。